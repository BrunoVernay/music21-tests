from music21 import stream, meter, tempo, chord, instrument, key, note, duration
import random

# Directly from [Sight Reading Music Generator](
# https://medium.com/@sumit.tripathi/sight-reading-music-generator-2d0f48ec4405)


def generate_random_song():
    # Random values for demonstration
    time_signature = "4/4"
    bpm = random.randint(60, 120)
    num_bars = 8

    # Create a stream object with the given time signature
    ts = meter.TimeSignature(time_signature)
    song_stream = stream.Score()
    song_stream.insert(0, ts)

    # Set the tempo in beats per minute
    song_stream.insert(0, tempo.MetronomeMark(number=bpm))

    # Generate a random chord progression (for demonstration, using only major and minor chords)
    chord_progression = [note.Note(root) for root in
                         random.choices(['C4', 'D4', 'E4', 'F4', 'G4', 'A4', 'B4'], k=num_bars)]

    # Generate random notes for each bar aligned with the chord progression
    for i in range(num_bars):
        # Create a measure
        measure = stream.Measure()

        # Add a chord to the measure
        measure.append(chord_progression[i])

        # Generate random notes based on the key of the chord
        current_key = chord_progression[i].nameWithOctave
        for j in range(random.randint(4, 8)):  # Random number of notes per bar
            note_duration = random.choice([0.25, 0.5, 1.0])  # Random note duration
            note_pitch = random.choice(key.Key(current_key).getScale().getPitches())
            note_obj = note.Note(note_pitch)
            note_obj.duration = duration.Duration(note_duration)
            measure.append(note_obj)

        # Append the measure to the song stream
        song_stream.append(measure)

    # Add an instrument (for demonstration, using piano)
    song_stream.insert(0, instrument.Piano())

    return song_stream


if __name__ == "__main__":
    generated_song = generate_random_song()

    # Save as MusicXML for MuseScore
    notation_musescore_filename = "random_song_with_chords.musicxml"
    generated_song.write('musicxml', fp=notation_musescore_filename)

    print(f"Notation file '{notation_musescore_filename}' (MuseScore) has been created with a random song.")
