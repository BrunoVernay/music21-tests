# Music21 tests


## Description

Testing [Music21 Python lib](https://web.mit.edu/music21/).  See [Code Meowstro - YouTube Playlist](https://www.youtube.com/playlist?list=PL1dIy2tm36HUn36wXyfXHauDFKO8LviCO) 

* The 'SightReading' script generates random number of notes and duration. The measures end up being messed up.
* The 'scales' script does work

TODO
* 'SightReading': Fix the measure size, do not generate random duration and number of notes.

## Installation

`sudo dnf5 install pipenv lilypond`

WARNING:: Do not install Music21 RPM! Unless you are using Toolbox or some kind of container. 
The RPM package has a ton of dependencies that you may not want in your system.


## Usage

Launch the script in your IDE. Or on command line:
```commandline
pipenv shell
python ./scales.py
exit
```

* `SightReading`: It works. Use MuseScore 4.2 to open the `.msxml` file and crashes  :-)
* `scales` should popup the image of a score
* 

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project:
- [Sight Reading Music Generator](https://medium.com/@sumit.tripathi/sight-reading-music-generator-2d0f48ec4405)


## Project status
* Starting ...
* ~~development has slowed down~~ 
* ~~development has stopped completely~~ 

