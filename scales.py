from music21 import scale, pitch, stream, note


# From https://www.youtube.com/watch?v=TSO0fB5Tess


def get_steps(from_pitch, to_pitch, origin_scale):
    """
    Calculates the distance in scale-steps between two pitches
    :param from_pitch:
    :param to_pitch:
    :param origin_scale:
    :return:
    """

    # Sort the from and to pitches in ascending order
    pitch_range = sorted([from_pitch, to_pitch])

    # List of all scales pitches within pitch_range
    pitches_in_range = origin_scale.getPitches(*pitch_range)

    # Calculate the number of steps
    steps = len(pitches_in_range) - 1

    # If the pitches descend, reverse the polarity
    if from_pitch > to_pitch:
        steps *= -1

    return steps

if __name__ == "__main__":
    my_scales1 = scale.CyclicalScale('C', ['M2', "m2", 'M2', "m2"])
    my_scales2 = scale.CyclicalScale('C', ['m3', "m2", 'm2', "m2"])

    # Create a list pf pitches based on my_scales1
    origin = [pitch.Pitch(p) for p in ['C4', 'E4', 'B3', 'A#4', 'G#4', 'E4']]

    # Create a list to contain the translated pitches
    translated = [origin[0]]

    # Iterate over the list of pitches
    for i in range(1, len(origin)):
        n_scale_steps = get_steps(origin[i - 1], origin[i], my_scales1)

        # Create a new pitch by traversing the scale
        new_pitch = my_scales2.nextPitch(translated[i - 1], n_scale_steps)
        translated.append(new_pitch)

    part1 = stream.Part([note.Note(p) for p in origin])
    part2 = stream.Part([note.Note(p) for p in translated])
    my_score = stream.Score([part1, part2])
    my_score.show('lily')

